
---------------------------------------------------------
-------   TP1 FENNICHE OUAHAB   -------------------------
---------------------------------------------------------
--Exercice 1

CREATE or replace FUNCTION nbre_exemplaires (o oeuvre) returns bigint
AS $$
	SELECT COUNT(*) 
	from livre 
	where id_oeuvre= $1.id_oeuvre;

$$ language sql;

--select o.titre, o.nbre_exemplaires
--from oeuvre o;

--Exercice 2

CREATE or replace FUNCTION est_emprunte (l livre) returns boolean
AS $$
	begin
		perform 1
		from emprunt e
		where $1.id_livre = e.id_livre;
		return found;
	end

$$ language plpgsql;

--select l.id_livre, o.titre, l.est_emprunte
--from livre l natural join oeuvre o;

--exercice 3

CREATE or replace  function livres_presents ()  returns setof int 
AS $$
	
	select id_livre
	from livre l
	where l.id_livre not in (
		select emprunt.id_livre
		from emprunt);

$$ language sql;

--select l.id_livre, o.titre
--from livre l, oeuvre o, livres_presents() as lp
--where l.id_livre = lp and l.id_oeuvre = o.id_oeuvre;

--exercice 4

CREATE FUNCTION nbre_emprunts (o oeuvre) returns bigint 
as $$
	declare
		nbe int;
	begin 
		select count(id_oeuvre) 
			into nbe
			from livre natural join emprunt
			where $1.id_oeuvre= id_oeuvre;
		if found then 
			return nbe;
		else
			return 0;
		end if;
	end;
$$language plpgsql;

--select o.titre, o.nbre_emprunts
--from oeuvre o;		

--exercice 5
create or replace function nbre_emprunts_histo(o oeuvre) returns bigint
as $$
	declare
		nbe int;
	begin	
		select count(id_oeuvre)
			into nbe
			from livre natural join histoemprunt
			where $1.id_oeuvre = id_oeuvre;
		if found then
			return nbe;
		else
			return 0;
		end if;
	end;
$$ language plpgsql;

CREATE or REPLACE function les_plus_empruntes(n int) returns table(titre text)
as $$
    select o.titre
    from oeuvre o
    order by (o.nbre_emprunts + o.nbre_emprunts_histo) desc, o.titre asc
    limit $1;
$$ language sql;

--select les_plus_empruntes(3) as titre;

--Exercice 6

CREATE  or REPLACE function infos_oeuvre(id_oeuvre int) returns varchar
as $$ 
	declare
    	ligne record;
        _auteur record;
		auteurs varchar (100);
        aff varchar (100);
		nbauteurs int;
	begin
    	select *
			into ligne
			from oeuvre, auteur, oeuvreauteur
			where oeuvre.id_oeuvre = oeuvreauteur.id_oeuvre
      			and oeuvreauteur.id_auteur = auteur.id_auteur
      			and oeuvre.id_oeuvre = $1;
      	auteurs := '';
		nbauteurs = 0;
        for _auteur in (select *
						from auteur, oeuvreauteur
						where oeuvreauteur.id_auteur = auteur.id_auteur
         					and oeuvreauteur.id_oeuvre = $1)
        	loop
			if (nbauteurs = 0) then
        		auteurs := auteurs || _auteur.nom_auteur;
			end if;
			nbauteurs := nbauteurs + 1;
        	end loop;
		if (nbauteurs = 1) then
        	aff := 'Titre : ' || ligne .titre || ' - Auteur : ' || auteurs;
		else
			aff := 'Titre : ' || ligne .titre || ' - Auteurs : ' || auteurs || ' et al';
    	end if;
		return aff;
	end
$$ language plpgsql;

--select id_oeuvre, infos_oeuvre(o.id_oeuvre) as infos_oeuvre
--from oeuvre o;

--Exercice 7

--alter table livre
--add empruntable boolean default 'true';

CREATE or REPLACE function est_empruntable(o oeuvre) returns boolean
as $$
    begin
        perform 1
        from livre l
        where o.id_oeuvre = l.id_oeuvre and l.empruntable;
        return found;
    end
$$ language plpgsql;

--select l.id_livre, o.titre, o.est_empruntable as empruntable
--from oeuvre o natural join livre l;

--Exercice 8

CREATE or REPLACE function sort_du_pret(o oeuvre) returns void
as $$
    update livre
    set empruntable = 'f'
    where (select count(id_livre)
           from livre l
           where $1.id_oeuvre = l.id_oeuvre) > 2
		and $1.est_empruntable
		and id_livre = (select id_livre
        				from livre l 
        				where $1.id_oeuvre = l.id_oeuvre
							and $1.est_empruntable
        				order by id_livre asc
        				limit 1)
$$ language sql;
--------------------------------------------------------------------
--                            TP2                                 --
--                    FENNICHE OUAHAB                             --
--------------------------------------------------------------------

-- EXERCICE 1 : 


create or replace function chiffreaffaires(categorie1 int) returns double precision
as $$
    declare
        s int;
    begin
        select sum(((date_fin - date_debut) + 1) * prix_par_jour)
        into s
        from categorie c, voiture v, contrat co, etat e
        where c.id_categorie = v.id_categorie
            and c.id_categorie = $1 
            and v.id_voiture = co.id_voiture 
            and co.id_etat = e.id_etat
            and e.intitule_etat = 'contrat termine';
        if (s > 0) then
            return s;
        else
            return 0 ;
        end if;
    end
$$ language plpgsql;


--EXERCICE 2 : 


create or replace function evalca(categorie int) returns varchar
as $$
    begin
    if(chiffreaffaires($1) = 0) then
        return 'inexistant';
    else
        if(chiffreaffaires($1) < 500) then
            return 'faible';
        else
            if(chiffreaffaires($1) < 1000) then
                return 'moyen';
            else
                return 'bon';
            end if;
        end if;
    end if;
    end;
$$ language plpgsql;


-- EXERCICE 3 : 


create or replace function ca_categorie() returns table(id_categorie integer,
                                                        libelle_categorie varchar(100),
                                                        chiffre_affaire double precision,
                                                        type_chiffre_affaire varchar(100))
as $$
    select id_categorie, libelle_categorie, chiffreaffaires(id_categorie),
        evalca(id_categorie)
    from categorie;
$$ language sql;



-- EXERCICE 4 : 



create or replace function sanslocation(date_d date, date_f date) returns int
as $$
    declare
        d date;
        nb int;
    begin
        d := date_d; -- date de debut
        nb := 0; -- nombre jours
        loop
            if d > date_f then
                return nb;
            end if;
            perform 1
            from contrat c, etat e
            where date_debut <= d 
                and date_fin >= d
                and c.id_etat = e.id_etat
                and (e.intitule_etat not like 'annulation')
		and (e.intitule_etat not like 'litige');

            if not found then
                nb := nb + 1;
            end if;

            d := d + 1;
        end loop;
    end
$$ language plpgsql;



-- EXERCICE 5 : 


create or replace function cblocation(date_d date, date_f date) returns int
as $$
    declare
        d date;
        nb int;
        v int;
    begin
        d := date_d;
        nb := 0;
        loop
            if d >= date_f then
                return nb;
            end if;
            select into v count(*)
            from contrat c, etat e
            where date_debut <= d 
                and date_fin >= d
                and c.id_etat = e.id_etat
                and (e.intitule_etat not like 'annulation');
            nb := nb + v;
            d := d + interval '1' day;
        end loop;
    end
$$ language plpgsql;

--Exercice 6

create or replace function prixlocation(id_contrat int) returns double precision
as $$
	select sum(((date_fin - date_debut + 1) * prix_par_jour))
    from voiture, contrat, categorie, etat
	where categorie.id_categorie = voiture.id_categorie
        and voiture.id_voiture = contrat.id_voiture
        and id_contrat = $1
		and contrat.id_etat = etat.id_etat 
$$ language sql;

--Exercice 7

create or replace function maj_prixlocation(c contrat) returns void
as $$
    update contrat c
    set prix_location = prixlocation(c.id_contrat); 
$$ language sql;

--Exercice 8

alter table client add ristourne double precision default 0.0;

--Exercice 9

create or replace function maj_ristourne() returns void
as $$
    declare
        i record;
        cpt integer;
    begin
        cpt := 0;
        for i in
            select cl.*, sum(co.prix_location) as valeur
            from client cl natural join contrat co
            group by id_client
            order by valeur desc
            limit 5
            loop
                cpt := cpt + 1;
                if cpt >= 1 and cpt <= 3 then
                    if i.ville like 'lens' then
                        update client cl
                        set ristourne = 0.08
                        where i.id_client = cl.id_client;
                    else
                        update client cl
                        set ristourne = 0.05
                        where i.id_client = cl.id_client;
                    end if;
                else
                    if i.ville like 'lens' then
                        update client cl
                        set ristourne = 0.05
                        where i.id_client = cl.id_client;
                    else
                        update client cl
                        set ristourne = 0.03
                        where i.id_client = cl.id_client;
                    end if;
                end if;
            end loop;
            end;
$$ language plpgsql;

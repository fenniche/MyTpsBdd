--------------------------------------------------------------------
--                            TP3                                 --
--                     FENNICHE OUAHAB                         --
--------------------------------------------------------------------

-- Exercice 1

CREATE or REPLACE function max6seances() returns trigger
as $$
	declare
		id_sem integer;
		nb_ticket integer;
		nb_reserv integer;
	begin
		select into id_sem id_semaine
		from seance s
		where new.id_seance = s.id_seance;

		if (select id_film
				from seance s
				where s.id_seance = new.id_seance
			) is null then
			raise notice 'La seance n existe pas';
			return null;
		end if;

		perform 1
		from abonnesemaine a
		where a.id_semaine = id_sem and new.id_abonne = a.id_abonne;

		if not found then
			raise notice 'L abonne % n est pas abonne a la semaine %', new.id_abonne, id_sem;
			return null;
		end if;

		select count(*)
		into nb_ticket
		from abonneseance natural join seance
		where id_semaine = id_sem and new.id_abonne = id_abonne;

		select count(*)
		into nb_reserv
		from (select id_abonne, id_seance
			      from reservation
			  except
			  select id_abonne, id_seance
			  	  from abonneseance
			  ) as resa
		where resa.id_abonne = new.id_abonne;

		if (nb_ticket + nb_reserv) = 6 then
			raise notice 'L abonne % a atteint le nombre maximum de places pour la semaine', new.id_abonne;
			return null;
		end if;
		
		return new;
	end
$$ language plpgsql;

drop trigger if exists tr_reserv_max6seances on reservation;
create trigger tr_reserv_max6seances
before insert or update
on reservation
for each row
execute procedure max6seances();

drop trigger if exists tr_abonne_max6seances on abonneseance;
create trigger tr_abonne_max6seances
before insert or update
on abonneseance
for each row
execute procedure max6seances();


-- Exercice 2

CREATE or REPLACE function verif_age() returns trigger
as $$
	declare
		age int;
	begin
		age := new.age;
		if (age < 16) then
			raise notice '% % est trop jeune pour s abonner', new.prenom_abonne, new.nom_abonne;
			return null;
		end if;
		if (age > 99) then
			new.age = 99;
			return new;
		end if;
		return new;
	end
$$ language plpgsql;


drop trigger if exists tr_verif_age on abonne;
create trigger tr_verif_age
before insert or update
on abonne
for each row
execute procedure verif_age();


-- Exercice 3

CREATE or REPLACE function verif_quota() returns trigger
as $$
	declare
		nb_reserv int;
		id_sal int;
		quota int;
	begin
		select count(*)
		into nb_reserv
		from reservation r
		where r.id_seance = new.id_seance;

		select s.id_salle
		into id_sal
		from seance s
		where s.id_seance = new.id_seance;

		select (s.resa_abonnes * s.nb_places / 100)
		into quota
		from salle s
		where s.id_salle = id_sal;

		if (quota = nb_reserv) then
			raise notice 'Le nombre maximum de reservations pour la seance % a deja ete atteint, l abonné % ne peut pas reserver', new.id_seance, new.id_abonne;
			return null;
		end if;

		return new;
	end
$$ language plpgsql;

drop trigger if exists tr_verif_quota on reservation;
create trigger tr_verif_quota
before insert or update
on reservation
for each row
execute procedure verif_quota();


-- Exercice 4

CREATE or REPLACE function resa_prise() returns trigger
as $$
	begin
		update reservation
		set prise = 'true'
		where id_abonne = new.id_abonne
		and id_seance = new.id_seance;
		return null;
	end
$$ language plpgsql;

drop trigger if exists tr_resa_prise on abonneseance;
create trigger tr_resa_prise
after insert or update
on abonneseance
for each row
execute procedure resa_prise();


-- Exercice 5

CREATE or REPLACE function max_places() returns trigger
as $$
	declare
		nb_plac_max int;
		nb_plac int;
		nb_reserv int;
	begin
		select nb_places
		into nb_plac_max
		from seance natural join salle
		where id_seance = new.id_seance;

		select count(*)
		into nb_plac
		from abonneseance
		where id_seance = new.id_seance;

		select count(*)
		into nb_reserv
		from reservation
		where id_seance = new.id_seance;

		if ((nb_plac + nb_reserv) = nb_plac_max) then
			raise notice 'Le nombre de places maximum pour la seance % a ete atteint', new.id_seance;
			return null;
		end if;

		return new;
	end
$$ language plpgsql;

drop trigger if exists tr_reserv_places_max on reservation;
create trigger tr_reserv_places_max
before insert or update
on reservation
for each row
execute procedure max_places();

drop trigger if exists tr_abonne_places_max on abonneseance;
create trigger tr_abonne_places_max
before insert or update
on abonneseance
for each row
execute procedure max_places();


-- Exercice 6

CREATE or REPLACE function controle_frequence_film() returns trigger
as $$
	declare
		id_sem_avant int;
		id_sem_cour int;
		id_sem_apres int;
		date_film date;
		nb_transmission_avant int;
		nb_transmission_cour int;
		nb_transmission_apres int;	
    begin
        select date_debut
		into date_film
        from semaine s
        where s.id_semaine = new.id_semaine;
        
        select id_semaine
		into id_sem_cour
        from semaine
        where date_debut <= date_film and date_fin >= date_film;
        
        select id_semaine
		into id_sem_avant
        from semaine
        where date_debut <= (date_film - 7) and date_fin >= (date_film - 7);
        
        select id_semaine
		into id_sem_apres
        from semaine
        where date_debut <= (date_film + 7) and (date_fin >= date_film + 7);
        
        if (id_sem_apres is not null) then
	        select count(*)
			into nb_transmission_cour
	        from seance
	        where id_film = new.id_film and (id_sem_cour = id_semaine or id_sem_apres = id_semaine);
	        
	        if (nb_transmission_cour = 10) then
	        	raise notice 'Le film a deja ete retransmis 10 fois entre les semaines % et %', id_sem_cour, id_sem_apres;
	       		return null;
	        end if;
        end if;

        if (id_sem_avant is not null) then
	        select count(*)
			into nb_transmission_cour
	        from seance
	        where id_film = new.id_film and (id_sem_cour = id_semaine or id_sem_avant = id_semaine);
	        
	        if (nb_transmission_cour = 10) then
	        	raise notice 'Le film a deja ete retransmis 10 fois entre les semaines % et %', id_sem_cour, id_sem_avant;
	       		return null;
	        end if;
        end if;

        return new;
    end
$$ language plpgsql;


drop trigger if exists tr_controle_frequence_film on seance; 
create trigger tr_controle_frequence_film
before insert or update
on seance
for each row
execute procedure controle_frequence_film();


-- Exercice 7

CREATE or REPLACE function deux_seances() returns trigger
as $$
	begin
		perform 1
		from seance natural join film 
		where id_salle = new.id_salle
			and (horaire_debut < new.horaire_debut and new.horaire_debut < horaire_debut + cast(dure + 20 || ' minutes' as interval))
			or (horaire_debut < new.horaire_debut + cast(dure + 20 || ' minutes' as interval)
			and new.horaire_debut + cast(dure + 20 || ' minutes' as interval) < horaire_debut + cast(dure + 20 || ' minutes' as interval));
		
		if found then
			raise exception 'Un film est deja programme dans cette salle a ce moment';
		end if;
		return new;
	end
$$ language plpgsql;

drop trigger if exists tr_deux_seances on seance;
create trigger tr_deux_seances
before insert or update on seance
for each row
execute procedure deux_seances();

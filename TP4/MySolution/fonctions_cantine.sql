-------------------------------------------------------------------------------------------------
----------------------  TP 4 BD -----------------------------------------------------------------
-------------------------- fenniche ouahab ------------------------------------------------------




--- Exercice 1 ----



--1 


--Une fonction qui retourne le nombre de tuples et le nombre de pages-disque d'une table entrée en argument. Utilisez cette fonction pour consulter les valeurs correspondantes de la table emp.


Create or Replace function nb_tuples_and_pages_table (name_table varchar) returns table(nb_tuples real, nb_pages integer)
AS $$
	select  reltuples , relpages from pg_class
	where relname = $1;

$$ language sql;

-- test : # select * from nb_tuples_and_pages_table('emp');

-- 2 

--Une fonction qui retourne les statistiques coorespodantes aux nombre de valeurs distinctes, les valeurs les plus courantes, et leur fréquence d'apparition pour une colonne et une table données en argument.

Create or replace function statistic_table(name_table varchar(50)) returns record  
AS $$ 
	select attname, most_common_vals , most_common_freqs 
	From pg_stats 
	where tablename=$1 and not (attname like 'id_%');


$$ language sql;

-- test : select statistic_table('emp');

-- 3

-- (a) Quelle type de < scan > sera réalisé sur la table ?

EXPLAIN select e1.*
from emp e1
where e1.sal in
(select e2.sal
from emp e2
where e1.id_chef = e2.id_emp);
 

-- Résulat :
--                            QUERY PLAN                           
----------------------------------------------------------------
 --Seq Scan on emp e1  (cost=0.00..1120236.50 rows=5000 width=12)
   --Filter: (SubPlan 1)
  -- SubPlan 1
    -- ->  Seq Scan on emp e2  (cost=0.00..224.00 rows=1 width=4)
         --  Filter: ($0 = id_emp)
-- (5 lignes)


-- (b) Combien de pages_disque ?

-- 1120236.50

-- (c) Combien des lignes en sortie

--5000

-- 4 


--  Proposez une nouvelle requête plus efficace pour répondre à la même question et analysez son plan d’exécution.

select * from emp e1  join emp e2 on e1.id_chef = e2.id_emp
where e1.sal=e2.sal;
 
-- (a)   SEQ
--                          QUERY PLAN                              
----------------------------------------------------------------------
 --Hash Join  (cost=224.75..456.99 rows=174 width=12)
  -- Hash Cond: ((e1.sal = e2.sal) AND (e1.id_chef = e2.id_chef))
  -- ->  Seq Scan on emp e1  (cost=0.00..224.00 rows=50 width=12)
    --     Filter: (id_chef = id_emp)
  -- ->  Hash  (cost=224.00..224.00 rows=50 width=12)
       --  ->  Seq Scan on emp e2  (cost=0.00..224.00 rows=50 width=12)
            --   Filter: (id_chef = id_emp)
-- (7 lignes)

-- (b) 456.99

-- (c) 174

-- 5 


-- Créez un index pour la colonne id_emp

create index test_idx on emp (id_emp);

-- Oui cela améliore la performance de ma requette car  ceci est plus efficace qu’une lecture complète de la table





------------------------------------------------------------------------------------------------
------------------ EXERCICE 2 -----------------------------------------------------------------
--------------------------------------------------------------------------------------------------




-- Question 1

create or replace function descr_tables() returns table(tablename name, nb_tuple real, nb_pages integer)
as $$
	select distinct tablename, reltuples, relpages 
	from pg_class c right outer join pg_tables t on t.tablename = c.relname 
	where tableowner = "current_user"();
$$ language sql;


-- Question 2

create or replace function descr_val(t varchar, col varchar) returns table(dist real, cour text, freq real[])
as $$
	select n_distinct, most_common_vals::text, most_common_freqs
	from pg_stats
	where tablename = $1 and attname = $2;
$$ language sql;

 
-- Question 3

/*
HashAggregate  (cost=5335868.67..5335968.66 rows=9999 width=8)
   Group Key: e.nom_enfant
   ->  Nested Loop  (cost=5.93..5335602.67 rows=106399 width=8)
         ->  Nested Loop  (cost=0.00..6699.68 rows=534000 width=16)
               ->  Seq Scan on insc_matin m  (cost=0.00..15.00 rows=1000 width=8)
               ->  Materialize  (cost=0.00..11.01 rows=534 width=8)
                     ->  Seq Scan on insc_soir s  (cost=0.00..8.34 rows=534 width=8)
         ->  Bitmap Heap Scan on enfant e  (cost=5.93..9.97 rows=1 width=12)
               Recheck Cond: ((id_enfant = s.id_enfant) OR (id_enfant = m.id_enfant))
               Filter: (((id_enfant = s.id_enfant) AND (s.date_inscr = '2011-11-02'::date)) OR ((id_enfant = m.id_enfant) AND (m.date_inscr = '2011-11-02'::date)))
               ->  BitmapOr  (cost=5.93..5.93 rows=2 width=0)
                     ->  Bitmap Index Scan on enfant_pkey  (cost=0.00..0.52 rows=1 width=0)
                           Index Cond: (id_enfant = s.id_enfant)
                     ->  Bitmap Index Scan on enfant_pkey  (cost=0.00..0.41 rows=1 width=0)
                           Index Cond: (id_enfant = m.id_enfant)
(15 lignes)
*/

-- (a) 5 335 868,67 accès aux pages disque, 5335868.67 en préparation

-- (b) 9999 lignes en sortie

-- (c) Les Nested Loop join


-- Question 4

explain select nom_enfant
from enfant 
where id_enfant in (
	select id_enfant
	from insc_soir
	where date_inscr = '2011-11-02'
	) or id_enfant in (
	select id_enfant
	from insc_matin
	where date_inscr = '2011-11-02'
	);

/*
Seq Scan on enfant  (cost=27.56..232.54 rows=7499 width=8) (actual time=0.210..2.479 rows=133 loops=1)
   Filter: ((hashed SubPlan 1) OR (hashed SubPlan 2))
   Rows Removed by Filter: 9867
   SubPlan 1
     ->  Seq Scan on insc_soir  (cost=0.00..9.68 rows=53 width=4) (actual time=0.021..0.052 rows=53 loops=1)
           Filter: (date_inscr = '2011-11-02'::date)
           Rows Removed by Filter: 481
   SubPlan 2
     ->  Seq Scan on insc_matin  (cost=0.00..17.50 rows=100 width=4) (actual time=0.043..0.104 rows=100 loops=1)
           Filter: (date_inscr = '2011-11-02'::date)
           Rows Removed by Filter: 900
 Planning time: 0.096 ms
 Execution time: 2.557 ms
(13 lignes)
*/

-- (a) 232.3 accès aux pages disques, 27.56 en préparation

-- (b) 7499 lignes en sortie

-- (c) Aucune jointure utilisée


-- Question 5

-- Dans la première requête, on vérifie pour chaque enfant s'il est inscrit à la cantine le soir.
-- On doit vérifier une deuxième fois s'il est inscrit le matin pour la date désirée car il y a une
-- jointure entre la table enfant et les tables inscr_matin et inscr_soir

-- Dans notre requête, on retourne seulement les id des enfants inscrits le soir ou le matin
-- pour la date désirée, et on vérifie pour chaque enfant s'il se trouve parmi ceux ci
-- On n'a donc pas à vérifier toute la table pour chaque enfant
-- On vérifie seulement si la date est bonne


-- Question 6

-- Mon camarade a les résultats suivants :

/*
HashAggregate  (cost=295.24..314.17 rows=1893 width=8) (actual time=6.433..6.447 rows=133 loops=1)
   Group Key: e.nom_enfant
   ->  Hash Left Join  (cost=42.52..290.51 rows=1893 width=8) (actual time=0.481..6.394 rows=133 loops=1)
         Hash Cond: (e.id_enfant = s.id_enfant)
         Filter: ((s.date_inscr = '2011-11-02'::date) OR (m.date_inscr = '2011-11-02'::date))
         Rows Removed by Filter: 9867
         ->  Hash Left Join  (cost=27.50..229.99 rows=9999 width=16) (actual time=0.311..4.200 rows=10000 loops=1)
               Hash Cond: (e.id_enfant = m.id_enfant)
               ->  Seq Scan on enfant e  (cost=0.00..154.99 rows=9999 width=12) (actual time=0.005..1.261 rows=10000 loops=1)
               ->  Hash  (cost=15.00..15.00 rows=1000 width=8) (actual time=0.298..0.298 rows=1000 loops=1)
                     Buckets: 1024  Batches: 1  Memory Usage: 40kB
                     ->  Seq Scan on insc_matin m  (cost=0.00..15.00 rows=1000 width=8) (actual time=0.004..0.113 rows=1000 loops=1)
         ->  Hash  (cost=8.34..8.34 rows=534 width=8) (actual time=0.138..0.138 rows=534 loops=1)
               Buckets: 1024  Batches: 1  Memory Usage: 21kB
               ->  Seq Scan on insc_soir s  (cost=0.00..8.34 rows=534 width=8) (actual time=0.004..0.059 rows=534 loops=1)
 Planning time: 0.257 ms
 Execution time: 6.488 ms
(17 lignes)
*/

-- Sa solution est plus efficace en nombre de lignes en sortie car il fait des jointures
-- entre les tables enfant, insc_soir et insc_matin : il vérifie dans la requête que les
-- enfants sont bien inscrits à la cantine

-- Mais elle est moins efficace en temps d'exécution et d'accès aux pages disque
-- car il doit à chaque fois vérifer toute la table insc_soir et toute la table insc_matin




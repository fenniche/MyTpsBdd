--- definition des tables

create table enfant(
	id_enfant serial primary key,
	nom_enfant varchar(8));
create table insc_matin(
       	id_enfant int references enfant,
       	date_inscr date,
	constraint insc_matin_pk primary key (id_enfant, date_inscr));
create table insc_soir(
       	id_enfant int references enfant,
       	date_inscr date,
	constraint insc_soir_pk primary key (id_enfant, date_inscr));


--- definition de la fonction
--- pour remplir les tables

create or replace function creer_enregs10000() returns void as '
declare
  i int := 0;
  j int;
  la_date date;
  k int := 0;
begin
  loop
    exit when i>=10000;
    insert into enfant(nom_enfant) values (''toto''||i);
    i := i+1;
  end loop;
  la_date := cast (''2011-10-29'' as date);
  i:=1;
  loop 
    exit when i>100;
    j := 0;
    loop 
      exit when j>=10000;
      insert into insc_matin(id_enfant, date_inscr) values (i+j,la_date);
      if (k%3 = 0) then insert into insc_soir(id_enfant, date_inscr) values (i+j+4,la_date);
      end if;
      if (k%5 = 0) then insert into insc_soir(id_enfant, date_inscr) values (i+j,la_date);
      end if;
      j := j+100;
      k := k+1;
    end loop;
    i := i+10;
    la_date = la_date + cast (''1 day'' as interval);
  end loop; 
  return;
end; ' language 'plpgsql';

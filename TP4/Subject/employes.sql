--- definition de la table

create table emp(
       id_emp int,
       sal int,
       id_chef int);


--- definition des fonctions
--- pour remplir la table

create or replace function creer_emp_nb(n int) returns void as '
declare
  i int := 1;
  salaire int;
begin
  loop
    exit when i>n;
    if (i%4) = 0 then
       salaire := 15000;
    elsif (i%4) = 1 then
       salaire := 7500;
    elsif (i%4) = 2 then
       salaire := 11800;
    else
       salaire := 10450;
    end if;
    insert into emp(id_emp,sal) values(i,salaire);
    i := i+1;
  end loop;
  update emp
  set id_chef = 1
  where (id_emp%6 = 2) or (id_emp%6 = 5);
  update emp
  set id_chef = 2
  where (id_emp%6 = 1) or (id_emp%6 = 4);
  update emp
  set id_chef = 3
  where (id_emp%6 = 0);
  update emp
  set id_chef = 4
  where (id_emp%6 = 3);
  return;
end; ' language 'plpgsql';

--- appel de la fonction
select creer_emp_nb(10000);
